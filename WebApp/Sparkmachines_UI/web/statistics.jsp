<%-- 
    Document   : statistics
    Created on : Nov 20, 2016, 2:41:28 PM
    Author     : S525118
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="home page css.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        
        <div class="right"> <b> <a href="intro.jsp"> <font color="white"> Home </font> </a> </b> </div>
        
         <header>
            
            <center>    <h1> <b> <font color="white">  BASEBALL PREDICTION </font> </b> </h1></center>
                      
        </header>
        
             
        <h1>    Statistics  </h1>
        
        <table>
            
            <tr> 
                 <td> <font color="white"> Player name  </font> </td> 
                 <td> <font color="white"> TEAM  </font> </td> 
                 <td> <font color="white"> AR  </font> </td> 
                 <td> <font color="white"> H  </font> </td> 
                 <td> <font color="white"> R  </font> </td> 
                 <td> <font color="white"> RBI </font> </td> 
                 <td> <font color="white"> BB </font> </td> 
                  <td> <font color="white"> SO </font> </td> 
                  <td> <font color="white"> PA </font> </td>
                   <td> <font color="white"> BA </font> </td>
                     <td> <font color="white"> OPS </font> </td>
            </tr>
       
            <tr>
               
               <%--
                    This code is to display the names of the players selected. 
                    --%>  
                
                
           <td> <font color="white"> 
               
                <%ArrayList<String> playernameSet = (ArrayList<String>)request.getAttribute("nam");
	for(int count1=0; count1<playernameSet.size();count1++){        
	   %>  
           <%= playernameSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            <%--
                    This code is to display the team names of the players selected. 
                    --%>  
             <td> <font color="white"> 
               
                <%ArrayList<String> teamvalstatSet = (ArrayList<String>)request.getAttribute("teamv");
	for(int count1=0; count1<teamvalstatSet.size();count1++){        
	   %>  
           <%= teamvalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
            <%--
                    This code is to display the 's' statistics of the players selected. 
                    --%>  
             <td> <font color="white"> 
                    <%ArrayList<String> playerResultSet = (ArrayList<String>)request.getAttribute("s");
	for(int count=0; count<playerResultSet.size();count++){        
	   %>  
           <%= playerResultSet.get(count)%>  <br> 
       <%  }%>
       </font> </td>
              <%--
                    This code is to display the 'hv' statistics of the players selected. 
                    --%>  
             <td> <font color="white"> 
               
                <%ArrayList<String> hvalstatSet = (ArrayList<String>)request.getAttribute("hv");
	for(int count1=0; count1<hvalstatSet.size();count1++){        
	   %>  
           <%= hvalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
             <%--
                    This code is to display the 'rv' statistics of the players selected. 
                    --%>  
            
            <td> <font color="white"> 
               
                <%ArrayList<String> rvalstatSet = (ArrayList<String>)request.getAttribute("rv");
	for(int count1=0; count1<rvalstatSet.size();count1++){        
	   %>  
           <%= rvalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
             <%--
                    This code is to display the 'rbiv' statistics of the players selected. 
                    --%>  
            
              <td> <font color="white"> 
               
                <%ArrayList<String> rbivalstatSet = (ArrayList<String>)request.getAttribute("rbiv");
	for(int count1=0; count1<rbivalstatSet.size();count1++){        
	   %>  
           <%= rbivalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            
             <%--
                    This code is to display the 'bbv' statistics of the players selected. 
                    --%>  
             <td> <font color="white"> 
               
                <%ArrayList<String> bbvalstatSet = (ArrayList<String>)request.getAttribute("bbv");
	for(int count1=0; count1<bbvalstatSet.size();count1++){        
	   %>  
           <%= bbvalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            
             <%--
                    This code is to display the 'sov' statistics of the players selected. 
                    --%>  
            
             <td> <font color="white"> 
               
                <%ArrayList<String> sovalstatSet = (ArrayList<String>)request.getAttribute("sov");
	for(int count1=0; count1<sovalstatSet.size();count1++){        
	   %>  
           <%= sovalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            
             <%--
                    This code is to display the 'pav' statistics of the players selected. 
                    --%>  
            <td> <font color="white"> 
               
                <%ArrayList<String> pavalstatSet = (ArrayList<String>)request.getAttribute("pav");
	for(int count1=0; count1<pavalstatSet.size();count1++){        
	   %>  
           <%= pavalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
             <%--
                    This code is to display the 'bav' statistics of the players selected. 
                    --%>  
            <td> <font color="white"> 
               
                <%ArrayList<String> bavalstatSet = (ArrayList<String>)request.getAttribute("bav");
	for(int count1=0; count1<bavalstatSet.size();count1++){        
	   %>  
           <%= bavalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
             <%--
                    This code is to display the 'opsv' statistics of the players selected. 
                    --%>  
             <td> <font color="white"> 
               
                <%ArrayList<String> opsvalstatSet = (ArrayList<String>)request.getAttribute("opsv");
	for(int count1=0; count1<opsvalstatSet.size();count1++){        
	   %>  
           <%= opsvalstatSet.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
            
            
            
            
            
            
                     
            </tr>
            
            
            
            <br>          
            
            <tr>   
                 <%--
                    This code is to display the names of the players selected. 
                    --%>  
                 <td> <font color="white"> 
               
                <%ArrayList<String> playernameSet1 = (ArrayList<String>)request.getAttribute("nam1");
	for(int count=0; count<playernameSet1.size();count++){        
	   %>  
           <%= playernameSet1.get(count)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
             <%--
                    This code is to display the team names of the players selected. 
                    --%>  
              <td> <font color="white"> 
               
                <%ArrayList<String> teamvalstatSet1 = (ArrayList<String>)request.getAttribute("teamv1");
	for(int count1=0; count1<teamvalstatSet1.size();count1++){        
	   %>  
           <%= teamvalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
            
            <%--
                    This code is to display the 's' statistics of the players selected. 
                    --%>
            
            <td> <font color="white">
             <%ArrayList<String> playerResultSet1 = (ArrayList<String>)request.getAttribute("s1");
	for(int count=0; count<playerResultSet1.size();count++){        
	   %>  
           <%= playerResultSet1.get(count)%>  <br>
       <%  }%>
       </font>   </td>
            
                <%--
                    This code is to display the 'hv' statistics of the players selected. 
                    --%>
            `
             <td> <font color="white"> 
               
                <%ArrayList<String> hvalstatSet1 = (ArrayList<String>)request.getAttribute("hv1");
	for(int count1=0; count1<hvalstatSet1.size();count1++){        
	   %>  
           <%= hvalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            <%--
                    This code is to display the 'rv' statistics of the players selected. 
                    --%>
            
             <td> <font color="white"> 
               
                <%ArrayList<String> rvalstatSet1 = (ArrayList<String>)request.getAttribute("rv1");
	for(int count1=0; count1<rvalstatSet1.size();count1++){        
	   %>  
           <%= rvalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
<%--
                    This code is to display the 'rbiv' statistics of the players selected. 
                    --%>
            
             <td> <font color="white"> 
               
                <%ArrayList<String> rbivalstatSet1 = (ArrayList<String>)request.getAttribute("rbiv1");
	for(int count1=0; count1<rbivalstatSet1.size();count1++){        
	   %>  
           <%= rbivalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            <%--
                    This code is to display the 'bbv' statistics of the players selected. 
                    --%>
            
             <td> <font color="white"> 
               
                <%ArrayList<String> bbvalstatSet1 = (ArrayList<String>)request.getAttribute("bbv1");
	for(int count1=0; count1<bbvalstatSet1.size();count1++){        
	   %>  
           <%= bbvalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            
            <%--
                    This code is to display the 'sov' statistics of the players selected. 
                    --%>
            
             <td> <font color="white"> 
               
                <%ArrayList<String> sovalstatSet1 = (ArrayList<String>)request.getAttribute("sov1");
	for(int count1=0; count1<sovalstatSet1.size();count1++){        
	   %>  
           <%= sovalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            <%--
                    This code is to display the 'pav' statistics of the players selected. 
                    --%>
            
            <td> <font color="white"> 
               
                <%ArrayList<String> pavalstatSet1 = (ArrayList<String>)request.getAttribute("pav1");
	for(int count1=0; count1<pavalstatSet1.size();count1++){        
	   %>  
           <%= pavalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td>  
            <%--
                    This code is to display the 'bav' statistics of the players selected. 
                    --%>
            
            
             <td> <font color="white"> 
               
                <%ArrayList<String> bavalstatSet1 = (ArrayList<String>)request.getAttribute("bav1");
	for(int count1=0; count1<bavalstatSet1.size();count1++){        
	   %>  
           <%= bavalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
            <%--
                    This code is to display the 'opsv' statistics of the players selected. 
                    --%>
            
             <td> <font color="white"> 
               
                <%ArrayList<String> opsvalstatSet1 = (ArrayList<String>)request.getAttribute("opsv1");
	for(int count1=0; count1<opsvalstatSet1.size();count1++){        
	   %>  
           <%= opsvalstatSet1.get(count1)%>  <br> 
       <%  }%>
       </font> 
                
            </td> 
            
                     
            
            
            </tr>
           
        </table>
                
                 <%@ include file="footer.jsp" %> 
        
    </body>
</html>
