/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S525118
 */
@WebServlet(urlPatterns = {"/selectingplayer"})
public class selectingplayer extends HttpServlet {
    private static final long serialVersionUID = 1L;
  Connection connection;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String team1 = request.getParameter("Team 1");
        String team2 = request.getParameter("Team 2");

         try{
             Class.forName("oracle.jdbc.driver.OracleDriver");
         }
          catch (ClassNotFoundException e) {
		 System.out.print(e.getMessage());
	} 
         /*
          this is the connection to oracle database and executing the query and passing the resultset to the jsp page, by using request dispatcher.
         
         */
        try{
            
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:tiger", "system", "password");

            PreparedStatement ps = con.prepareStatement("select distinct batting, team_id from playersname where team_id=?");

            ps.setString(1, team1);
            ResultSet rs = ps.executeQuery();
            System.out.println("executed");
            /* Printing column names */
            // ResultSetMetaData rsmd=rs.getMetaData();
            request.setAttribute("r", rs);
            System.out.println("attribute set");
            PreparedStatement ps1 = con.prepareStatement("select distinct batting, team_id from playersname where team_id=?");
            ps1.setString(1, team2);
            ResultSet rs1 = ps1.executeQuery();
            /* Printing column names */
            //  ResultSetMetaData rsmd1=rs1.getMetaData();

            request.setAttribute("r1", rs1);
            RequestDispatcher infoDispacth = request.getRequestDispatcher("playerSelection.jsp");
            infoDispacth.forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}