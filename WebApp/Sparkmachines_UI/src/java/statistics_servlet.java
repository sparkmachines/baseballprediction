/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author S525118
 */
@WebServlet(urlPatterns = {"/statistics_servlet"})
public class statistics_servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
            /* TODO output your page here. You may use following sample code. */
           
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        // Code for storing all the selected players in a array playerteam1.
        String[] playerTeam1 = new String[20];
            String plaTeam1[] = request.getParameterValues("checked"); 
            int i; 
            for (i=0;i<plaTeam1.length;i++){
                playerTeam1[i]=plaTeam1[i];
                 
              System.out.println("palyers are "+ playerTeam1);
                // System.out.println("palyers are"+playerTeam1);
            }
            
           
        String[] playerTeam2 = new String[20];
            String plaTeam2[] = request.getParameterValues("checked1"); 
            int j;
            for (j=0;j<plaTeam2.length;j++){
                playerTeam2[j]=plaTeam2[j];
                
            }
        
         try{
             Class.forName("oracle.jdbc.driver.OracleDriver");
         }
          catch (ClassNotFoundException e) {
		 System.out.print(e.getMessage());
	} 

           /*
          this is the connection to oracle database and executing the query and passing the resultset to the jsp page, by using request dispatcher.
         
         */     
         try{
            
        Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:tiger","system","password");               

        PreparedStatement ps2=con.prepareStatement("select * from playersname where batting=?");
        int k;
        
        
        ArrayList<String> playerResults =new ArrayList();
        ArrayList<String> names=new ArrayList();
        ArrayList<String> hvalstat=new ArrayList();
        ArrayList<String> rvalstat=new ArrayList();
        ArrayList<String> rbivalstat=new ArrayList();
        ArrayList<String> bbvalstat=new ArrayList();
        ArrayList<String> sovalstat=new ArrayList();
        ArrayList<String> pavalstat=new ArrayList();
        ArrayList<String> bavalstat=new ArrayList();
         ArrayList<String> opsvalstat=new ArrayList();
         ArrayList<String> teamvalstat=new ArrayList();
        
        
        
        // for ar stats
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 ResultSet rs2 = ps2.executeQuery();
                 System.out.println("Reseults Set ::::"+rs2);
                                 
                 while(rs2.next())
                 {
                   String runs= rs2.getString("AR");                   
                   playerResults.add(runs);                   
                 }              
            }
             
            // for player name stats 
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet fornameval = ps2.executeQuery();
                 
                while(fornameval.next()){
                    String name= fornameval.getString("BATTING");
                  names.add(name);
                     
                }
             }
             
             
             // for h stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forhval = ps2.executeQuery();
                 
                while(forhval.next()){
                    String hval= forhval.getString("H");
                  hvalstat.add(hval);
                     
                }
             }
             
             // for r stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forrval = ps2.executeQuery();
                 
                while(forrval.next()){
                    String rval= forrval.getString("R");
                  rvalstat.add(rval);
                     
                }
             }
             // for rbi stats
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forrbival = ps2.executeQuery();
                 
                while(forrbival.next()){
                    String rbival= forrbival.getString("RBI");
                  rbivalstat.add(rbival);
                     
                }
             }
             
             // for bb stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forbbval = ps2.executeQuery();
                 
                while(forbbval.next()){
                    String bbval= forbbval.getString("BB");
                  bbvalstat.add(bbval);
                     
                }
             }
             
             
             // for so stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forsoval = ps2.executeQuery();
                 
                while(forsoval.next()){
                    String soval= forsoval.getString("SO");
                  sovalstat.add(soval);
                     
                }
             }
             
              // for so stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forpaval = ps2.executeQuery();
                 
                while(forpaval.next()){
                    String paval= forpaval.getString("PA");
                  pavalstat.add(paval);
                     
                }
             }
             
             // for ba
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forbaval = ps2.executeQuery();
                 
                while(forbaval.next()){
                    String baval= forbaval.getString("BA");
                  bavalstat.add(baval);
                     
                }
             }
             
             // for ops
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet foropsval = ps2.executeQuery();
                 
                while(foropsval.next()){
                    String opsval= foropsval.getString("OPS");
                  opsvalstat.add(opsval);
                     
                }
             }
             
              // for team names
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forteamval = ps2.executeQuery();
                 
                while(forteamval.next()){
                    String teamval= forteamval.getString("TEAM_ID");
                  teamvalstat.add(teamval);
                     
                }
             }
              
                  request.setAttribute("s", playerResults); 
                  request.setAttribute("nam", names); 
                   request.setAttribute("hv", hvalstat); 
                     request.setAttribute("rv", hvalstat);
                     request.setAttribute("rbiv", rbivalstat);
                  request.setAttribute("bbv", bbvalstat);
                  request.setAttribute("sov", sovalstat);  
                   request.setAttribute("pav", pavalstat);  
                  request.setAttribute("bav", bavalstat); 
                   request.setAttribute("opsv", opsvalstat); 
                   request.setAttribute("teamv", teamvalstat); 
                  
                  
                     PreparedStatement ps3=con.prepareStatement("select * from playersname where batting=?");
                     
                     
                     ArrayList<String> playerResults1 =new ArrayList();
                     ArrayList<String> names1 =new ArrayList();
                     ArrayList<String> hvalstat1=new ArrayList();
                     ArrayList<String> rvalstat1=new ArrayList();
                     ArrayList<String> rbivalstat1=new ArrayList();
                     ArrayList<String> bbvalstat1=new ArrayList();
                     ArrayList<String> sovalstat1=new ArrayList();
                     ArrayList<String> pavalstat1=new ArrayList();
                     ArrayList<String> bavalstat1=new ArrayList();
                     ArrayList<String> opsvalstat1=new ArrayList();
                      ArrayList<String> teamvalstat1=new ArrayList();
                      
                      int l;
                       // for ar stats in team 2
                       
            for (l=0;l<plaTeam2.length;l++){
                playerTeam2[l]=plaTeam2[l];
                 ps3.setString(1,playerTeam2[l]); 
                     ResultSet rs3=ps3.executeQuery(); 
                                
                  while (rs3.next()) {
                    String runs1 = rs3.getString("AR");
                    playerResults1.add(runs1);
                }
            } 
            
            // for player names in team 2
            
                  for (l=0;l<plaTeam2.length;l++){
                playerTeam2[l]=plaTeam2[l];
                 ps3.setString(1,playerTeam2[l]); 
                     ResultSet fornameval1=ps3.executeQuery();
                                                     
                  while (fornameval1.next()) {
                    String name1 = fornameval1.getString("BATTING");
                    names1.add(name1);
                }
                }
            
            // for h values
            
             for (l=0;l<plaTeam2.length;l++){
                playerTeam2[l]=plaTeam2[l];
                 ps3.setString(1,playerTeam2[l]); 
                     ResultSet forhval1=ps3.executeQuery();
                                                     
                  while (forhval1.next()) {
                    String hval = forhval1.getString("H");
                    hvalstat1.add(hval);
                }
                }
            
            // for r val
            
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forrval1 = ps2.executeQuery();
                 
                while(forrval1.next()){
                    String rval1= forrval1.getString("R");
                  rvalstat1.add(rval1);
                     
                }
             }
            
              
              
              // for rbi stats
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forrbival1 = ps2.executeQuery();
                 
                while(forrbival1.next()){
                    String rbival1= forrbival1.getString("RBI");
                  rbivalstat1.add(rbival1);
                     
                }
             }
              
              
             // for bb stats in team 1
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forbbval1 = ps2.executeQuery();
                 
                while(forbbval1.next()){
                    String bbval1= forbbval1.getString("BB");
                  bbvalstat1.add(bbval1);
                     
                }
             }
             
             // for so stats in team 2
             
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forsoval1 = ps2.executeQuery();
                 
                while(forsoval1.next()){
                    String soval1= forsoval1.getString("SO");
                  sovalstat1.add(soval1);
                     
                }
             }
             
             for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forpaval1 = ps2.executeQuery();
                 
                while(forpaval1.next()){
                    String paval1= forpaval1.getString("PA");
                  pavalstat1.add(paval1);
                     
                }
             }
             
              // for ba
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forbaval1 = ps2.executeQuery();
                 
                while(forbaval1.next()){
                    String baval1= forbaval1.getString("BA");
                  bavalstat1.add(baval1);
                     
                }
             }
              
               // for ops
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet foropsval1 = ps2.executeQuery();
                 
                while(foropsval1.next()){
                    String opsval1= foropsval1.getString("OPS");
                  opsvalstat1.add(opsval1);
                     
                }
             }
              // for team names
             
              for (k = 0; k < plaTeam1.length; k++) {
                 playerTeam1[k] = plaTeam1[k];
                 
                 ps2.setString(1, playerTeam1[k]);
                 System.out.println("Prepared Statement:::"+ps2);
                 
                 ResultSet forteamval1 = ps2.executeQuery();
                 
                while(forteamval1.next()){
                    String teamval1= forteamval1.getString("TEAM_ID");
                  teamvalstat1.add(teamval1);
                     
                }
             }
              
              
              
                   request.setAttribute("s1",playerResults1);  
                    request.setAttribute("nam1",names1);
                    request.setAttribute("hv1", hvalstat1);
                    request.setAttribute("rv1", rvalstat1);
                    request.setAttribute("rbiv1", rbivalstat1);
                     request.setAttribute("bbv1", bbvalstat1);
                     request.setAttribute("sov1", sovalstat1); 
                     request.setAttribute("pav1", pavalstat1);
                     request.setAttribute("bav1", bavalstat1);
                      request.setAttribute("opsv1", opsvalstat1);
                request.setAttribute("teamv1", teamvalstat1);
                  
               	RequestDispatcher infoDispacth = request.getRequestDispatcher("statistics.jsp");
		infoDispacth.forward(request, response);
			

        
            }
         catch(SQLException e)
        {
            
           e.printStackTrace();
        } 
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
