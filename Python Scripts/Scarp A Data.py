from bs4 import BeautifulSoup
import urllib
import os
toOpenUrl=urllib.urlopen("http://www.baseball-reference.com/teams/")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find('table', id='active')
table_Body=tables.tbody
dataCmptly=""
for rows in table_Body.findAll('tr'):
    cellInRow=""
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text
    dataCmptly=dataCmptly+"\n"+cellInRow[1:]

print(dataCmptly)
header = "RK,Franchise,From,To,Games,Win,Lose,W-L%,G>.500,Divs,Pnnts,WS,Playsiffs,Players,HOF,R,AB,H,HR,BA,RA,ERA"+"\n"
file = open(os.path.expanduser("Baseball.text"), "wb")
file.write(bytes(header, encoding="ascii", errors="ignore"))
file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))

