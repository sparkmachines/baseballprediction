from bs4 import BeautifulSoup, Comment
import os
import urllib
header = "Date(MM/DD/YYYY),Team_Id,Dummy,Batting,AR,R,H,RBI,BB,SO,PA,BA,OBP,SLG,OPS,Pit,Str,WPA,aLI,WPA+,WPA-,RE24,PO,A"+"\n"
file = open(os.path.expanduser("w2011.csv"), "wb")
file.write(bytes(header))
year="2011"
day=["05","06","07","08"]
month=["03","04","05","06","07","08","09","10"]
teams=["BAL/BAL","ARI/ARI","BOS/BOS","ATL/ATL","CHW/CHW","CIN/CIN","CHC/CHC","CLE/CLE","COL/COL","DET/DET","FLA/FLA","HOU/HOU","KCR/KCR","TOR/TOR","TEX/TEX","TBD/TBD","SIT/SIT","SEA/SEA","SFG/SFG","SDP/SDP","PIT/PIT","PHI/PHI","OAK/OAK","NYY/NYY","NYM/NYM","MIN/MIN","MIL/MIL","SIT/SIT","SEA/SEA","LAD/LAD","ANA/ANA"]
for t in range(len(teams)):
    for j in range(len(month)):
        for k in range(len(day)):
            url = "http://www.baseball-reference.com/boxes/"+teams[t]+year+month[j]+day[k]+"0.shtml"
            print(url)
            tableId=["AnaheimAngelsbatting","ArizonaDiamondbacksbatting","ColoradoRockiesBatting","KansasCityRoyalsbatting","DetroitTigersbatting", "NewYorkYankeesbatting", "MilwaukeeBrewersbatting",
                                 "CincinnatiRedsbatting", "SanFranciscoGiantsbatting", "TorontoBlueJaysbatting""LosAngelesDodgersbatting", "SanDiegoPadresbatting","ChicagoWhiteSoxbatting","ClevelandIndiansbatting","BaltimoreOriolesbatting","TampaBayRaysbatting","MinnesotaTwinsbatting","TorontoBlueJaysbatting","BostonRedSoxbatting","TexasRangersbatting","SeattleMarinersbatting","OaklandAthleticsbatting","PittsburghPiratesbatting","ChicagoCubsbatting","HoustonAstrosbatting","PhiladelphiaPhilliesbatting",
                                  "AtlantaBravesbatting","WashingtonNationalsbatting","NewYorkMetsbatting","FloridaMarlinsbatting","ArizonaDiamondba"]
            page=urllib.urlopen(url)
            soup = BeautifulSoup(page,"html.parser")
            for z in range(len(tableId)):
                tables=soup.find('table', id=tableId[z])
                dataCmptly=""
                tablesplit=tableId[z][0:-7]
                try:
                    for rows in tables.findAll('tr'):
                        cellInRow = ""
                        for cells in rows.findAll('td'):
                            cellInRow = cellInRow + "," + cells.text
                        cellInRowWithDate = day[k]+"/"+month[j] +"/" + year + "," + tablesplit + "," + cellInRow+"\n"
                        print(cellInRowWithDate)
                        file.write(bytes(cellInRowWithDate))
                except:
                    print("Done")
