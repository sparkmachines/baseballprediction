import urllib.request
import urllib
# If you are using Python 3+, import urllib instead of urllib2

import json
from _socket import timeout
from re import error

from flask import logging

data =  {

        "Inputs": {

                "input1":
                {
                    "ColumnNames": ["Date", "Position", "Tm", "Venue", "Opp", "Results", "RS", "RA", "H", "Hopp", "E", "Eopp", "W", "L", "Inn", "Win", "Loss", "Save"],
                    "Values": [ [ "", "value", "value", "value", "value", "value", "0", "0", "0", "0", "0", "0", "0", "0", "0", "value", "value", "value" ], [ "", "value", "value", "value", "value", "value", "0", "0", "0", "0", "0", "0", "0", "0", "0", "value", "value", "value" ], ]
                },        },
            "GlobalParameters": {
}
    }

body = str.encode(json.dumps(data))

url = 'https://ussouthcentral.services.azureml.net/workspaces/19a2106a38d141e3a3d7b5fdb881b602/services/59f51f8af4564ac889abfcfb2c4f6079/execute?api-version=2.0&details=true'
api_key = 'UMoje30nts1zAmK+/6ouOAxYJh7K7NKgMnZEsKkCCBZKcqmHFLAvQ/7aB3gikVKZXNuCwpGhuorKZtAF/rVL1Q==' # Replace this with the API key for the web service
headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key)}

# req = urllib.request(url, body, headers)
req = urllib.request.Request(url, body, headers)
try:
    # response = urllib.request.urlopen(req)
    # response = urllib.request.urlopen(req)
    # If you are using Python 3+, replace urllib2 with urllib.request in the above code:
    # req = urllib.request.Request(url, body, headers)
    response = urllib.request.urlopen(req)
    result = response.read()
    print(result)
except (urllib.HTTPError, error):
    print("The request failed with status code: " + str(error.code))
except (timeout):
    logging.error('socket timed out - URL %s', url)

    # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
    print(error.info())

    print(json.loads(error.read()))