from bs4 import BeautifulSoup
import urllib
import urllib.request
import os
# year=str(2011)
# Stats ="batting"
#http://www.baseball-reference.com/leagues/MLB/2011-standard-batting.shtml
class dataScrap:
    def webScrapping(Self,year,Stats):
        link = "http://www.baseball-reference.com/leagues/MLB/" + year + "-standard-" + Stats + ".shtml"
        toOpenUrl=urllib.request.urlopen(link)
        print(link)
        urlLink=BeautifulSoup(toOpenUrl, "html.parser")
        if(Stats =="pitching"):
            tables=urlLink.find('table', id='players_standard_pitching')
        else:
            tables = urlLink.find('table', id='players_standard_batting')
        dataCmptly = ""
        for rows in tables.findAll('tr'):
            cellInRow = ""
            for cells in rows.findAll('td'):
                cellInRow = cellInRow + "," + cells.text
            dataCmptly = dataCmptly + "\n" + cellInRow
            print(dataCmptly)
        header = "Name,Age,Tm,Lg,W,L,W-L%,ERA,G,GS,GF,CG,SHO,SV,IP,H,R,ER,HR,BB,IBB,SO,HBP,BK,WP,BF,ERA+,FIP,WHIP,H9,HR9,BB9,SO9,SO/W"+"\n"
        file = open(os.path.expanduser("playerdata.csv"), "wb")
        file.write(bytes(header, encoding="ascii", errors="ignore"))
        file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))

abc=dataScrap()
abc.webScrapping(str(2011),"batting")
