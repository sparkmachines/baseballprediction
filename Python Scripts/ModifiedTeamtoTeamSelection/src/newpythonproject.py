# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
#from urllib.request import HTTPError
from urllib.error import HTTPError
import os
from bs4 import BeautifulSoup
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
import urllib
import urllib.request

app = Flask(__name__)
app.secret_key = 'keyForScrap'

@app.route('/')
def homepage():
    return render_template("yearandteams.html")

@app.route('/', methods=["GET", "POST"])
def mainpage():
    message = None
    if request.method == "POST":
        team1 = request.form['team1']
        team2 = request.form['team2']   
        fromYear = str(request.form['from'])
        to = str(request.form['to'])
        webScrapping(team1,team2,fromYear,to)
        message = "Done"       
    return render_template("yearandteams.html", message=message)
    
def webScrapping(team1,team2,fromYear,to):
    #http://www.baseball-reference.com/games/head2head-games.cgi?team1=LAD&team2=HOU&from=1901&to=2016&submit=Submit
    link = "http://www.baseball-reference.com/games/head2head-games.cgi?team1=" + team1+ "&team2=" + team2 + "&from=" + fromYear + "&to=" +to + "&submit=Submit"
    try:
        toOpenUrl=urllib.request.urlopen(link)
        urlLink=BeautifulSoup(toOpenUrl, "html.parser")
        tables=urlLink.find('table', id='game_results')
        dataCmptly=""
        for rows in tables.findAll('tr'):
            cellInRow = ""
            for cells in rows.findAll('td'):
                cellInRow=cellInRow+","+cells.text
            dataCmptly=dataCmptly+"\n"+cellInRow
        print(dataCmptly)
        header = "Dummy,Date,Standings,Team,Venue,Opposition,Result,Rs,Runs Allowed,Home Runs,Homes Runs Opp,E,E-Opp,Win,Loss,Inn,Win_Person,LosePerson,Save"+"\n"
        file = open(os.path.expanduser("TeamDataScrap.csv"), "wb")
        file.write(bytes(header, encoding="ascii", errors="ignore"))
        file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))
#        contents = resp.read()
    except urllib.error.URLError as e:
        contents = e.read()
#    print("Hello Python")
#    os.chdir("C:\\Users\\S525070\\Desktop\\pdi-ce-4.4.0-stable\\data-integration")
#    print('Directory has been changed')
#    os.system("BBclean.bat")
#    print('hello end of execution')
    #print("Hello World")
if __name__ == "__main__":
    app.run()

