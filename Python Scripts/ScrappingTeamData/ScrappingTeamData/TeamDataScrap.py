from bs4 import BeautifulSoup
import urllib
import urllib.request
import os
toOpenUrl=urllib.request.urlopen("http://www.baseball-reference.com/games/head2head-games.cgi?team1=BOS&team2=ARI&from=2001&to=2016&submit=Submit")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find('table', id='game_results')
dataCmptly=""
for rows in tables.findAll('tr'):
    cellInRow = ""
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text
    dataCmptly=dataCmptly+"\n"+cellInRow[1:]
    print(dataCmptly)
header = "Date,Standings,Team,Venue,Opposition,Result,Rs,Runs Allowed,Home Runs,Homes Runs Opp,E,E-Opp,Win,Loss,Inn,Win_Person,LosePerson,Save"+"\n"
file = open(os.path.expanduser("TeamDataScrap.csv"), "wb")
file.write(bytes(header, encoding="ascii", errors="ignore"))
file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))

