from bs4 import BeautifulSoup, Comment
import os
import urllib.request
header = "Date(MM/DD/YYYY),Team_Id,Dummy,Pitching,IP,H,R,ER,BB,SO,HR,ERA,BF,Pit,Str,Ctct,StS,StL,GB,FB,LD,Unk,GSc,IR,IS,WPA,aLI,RE24"+"\n"

file = open(os.path.expanduser("2012 pitching.csv"), "wb")
file.write(bytes(header, encoding="ascii", errors="ignore"))
year="2013"
day=["01","02","03"]
month=["03","04","05","06","07","08","09","10"]
teams=["BAL/BAL","ARI/ARI","BOS/BOS","ATL/ATL","CHW/CHW","CIN/CIN","CHC/CHC","CLE/CLE","COL/COL","DET/DET","FLA/FLA","HOU/HOU","KCR/KCR","TOR/TOR","TEX/TEX","TBD/TBD","SIT/SIT","SEA/SEA","SFG/SFG","SDP/SDP","PIT/PIT","PHI/PHI","OAK/OAK","NYY/NYY","NYM/NYM","MIN/MIN","MIL/MIL","LAD/LAD","ANA/ANA"]
for t in range(len(teams)):
    for j in range(len(month)):
        for k in range(len(day)):
            url = "http://www.baseball-reference.com/boxes/"+teams[t]+year+month[j]+day[k]+"0.shtml"
            print(url)
            tableId=["AnaheimAngelspitching","ArizonaDiamondbackspitching","ColoradoRockiespitching","KansasCityRoyalspitching","DetroitTigerspitching", "NewYorkYankeespitching", "MilwaukeeBrewerspitching",
                                 "CincinnatiRedspitching", "SanFranciscoGiantspitching", "TorontoBlueJayspitching""LosAngelesDodgerspitching", "SanDiegoPadrespitching"]
            page=urllib.request.urlopen(url)
            soup = BeautifulSoup(page,"html.parser")
            for z in range(len(tableId)):
                tables=soup.find('table', id=tableId[z])
                dataCmptly=""
                tablesplit=tableId[z][0:-7]
                try:
                    for rows in tables.findAll('tr'):
                        cellInRow = ""
                        for cells in rows.findAll('td'):
                            cellInRow = cellInRow + "," + cells.text
                        cellInRowWithDate = day[k]+"/"+month[j] +"/" + year + "," + tablesplit + "," + cellInRow+"\n"
                        print(cellInRowWithDate)
                        file.write(bytes(cellInRowWithDate, encoding="ascii", errors="ignore"))
                except:
                    print("Done")