#http://www.baseball-reference.com/boxes/BAL/BAL201106040.shtml

from bs4 import BeautifulSoup, Comment
import os
import urllib.request
header = "Date(DD/MM/YYYY),Team_Id,Dummy,Batting,AR,R,H,RBI,BB,SO,PA,BA,OBP,SLG,OPS,Pit,Str,WPA,aLI,WPA+,WPA-,RE24,PO,A"+"\n"
file = open(os.path.expanduser("DayToDay_2012.csv"), "wb")
file.write(bytes(header, encoding="ascii", errors="ignore"))
year="2012"
day=["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
month=["03","04","05","06","07","08","09","10"]
teams=["BOS/BOS","ATL/ATL","CHW/CHW","CIN/CIN","CHC/CHC","CLE/CLE","COL/COL","DET/DET","FLA/FLA","HOU/HOU","KCR/KCR","TOR/TOR","TEX/TEX","TBD/TBD","SIT/SIT","SEA/SEA","SFG/SFG"]
for t in range(len(teams)):
    for j in range(len(month)):
        for k in range(len(day)):
            url = "http://www.baseball-reference.com/boxes/"+teams[t]+year+month[j]+day[k]+"0.shtml"
            print(url)
            tableId=["AnaheimAngelsbatting","ArizonaDiamondbacksbatting","ColoradoRockiesBatting","KansasCityRoyalsbatting","DetroitTigersbatting", "NewYorkYankeesbatting", "MilwaukeeBrewersbatting",
                                 "CincinnatiRedsbatting", "SanFranciscoGiantsbatting", "TorontoBlueJaysbatting""LosAngelesDodgersbatting", "SanDiegoPadresbatting","ChicagoWhiteSoxbatting","ClevelandIndiansbatting","BaltimoreOriolesbatting","TampaBayRaysbatting","MinnesotaTwinsbatting","TorontoBlueJaysbatting","BostonRedSoxbatting","TexasRangersbatting","SeattleMarinersbatting","OaklandAthleticsbatting","PittsburghPiratesbatting","ChicagoCubsbatting","HoustonAstrosbatting","PhiladelphiaPhilliesbatting",
                                  "AtlantaBravesbatting","WashingtonNationalsbatting","NewYorkMetsbatting","FloridaMarlinsbatting","ArizonaDiamondba"]
            page=urllib.request.urlopen(url)
            soup = BeautifulSoup(page,"html.parser")
            for z in range(len(tableId)):
                tables=soup.find('table', id=tableId[z])
                dataCmptly=""
                tablesplit=tableId[z][0:-7]
                try:
                    for rows in tables.findAll('tr'):
                        cellInRow = ""
                        for cells in rows.findAll('td'):
                            cellInRow = cellInRow + "," + cells.text
                        cellInRowWithDate = day[k]+"/"+month[j] +"/" + year + "," + tablesplit + "," + cellInRow+"\n"
                        print(cellInRowWithDate)
                        file.write(bytes(cellInRowWithDate, encoding="ascii", errors="ignore"))
                except:
                    print("Done")