from bs4 import BeautifulSoup
import urllib
import os

k=""
m=""
def h3h(m,k):
    Url1 = urllib.urlopen("http://www.baseball-reference.com/games/head2head-games.cgi?team1="+m+"&team2="+k+"&from=1901&to=2016")
    soup1 = BeautifulSoup(Url1, "html.parser")
    tables2 = soup1.find("table", id="game_results")
    dataCmptly3 = ""
    for rows in tables2.findAll('tr'):
        cellInRow2 = ""
        for cells in rows.findAll('td'):
            cellInRow2 = cellInRow2 + "," + cells.text

        dataCmptly3 = dataCmptly3 + "\n" + cellInRow2[1:]

    header = "Date,Tm,Opp,RS,RA,H,Hopp,E,Eopp,W,L,Inn,Win,Loss,Save" + "\n"
    file = open(os.path.expanduser("Baseball"+m+"\"+k+"+".csv"), "wb")
    file.write(bytes(header))
    file.write(bytes(dataCmptly3))



def h2h(k):
    Url=urllib.urlopen("http://www.baseball-reference.com/games/head2head.cgi?teams="+k+"&from=1901&to=2016&submit=Submit")
    soup=BeautifulSoup(Url,"html.parser")
    tables1 = soup.find("table", id="")
    dataCmptly2=""
    for rows in tables1.findAll('tr'):
        cellInRow1 = ""
        for cells in rows.findAll('td'):
            cellInRow1 = cellInRow1 + "," + cells.text

        dataCmptly2 = dataCmptly2 + "\n" + cellInRow1[1:]

    header = "RK,Franchise,Tm,G,W,L,W-L%,RS,RA,hmW-L,rdW-L, " + "\n"
    file = open(os.path.expanduser("Baseball"+k+".csv"), "wb")
    file.write(bytes(header))
    file.write(bytes(dataCmptly2))


toOpenUrl=urllib.urlopen("http://www.baseball-reference.com/games/head2head.cgi?teams=ARI&from=1901&to=2016&submit=Submit")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find("table", id="")

dataCmptly=""
dataCmptly1=""

for rows in tables.findAll('tr'):
    cellInRow=""
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text

    dataCmptly=dataCmptly+"\n"+cellInRow[1:]
#print(dataCmptly)
for rows in tables.findAll('tr'):

    for cells in rows.findAll('td')[2:3]:

        k=cells.text
        if k=="LAA, ANA":
            k="ANA"
        if k=="MIA, FLA":
            k="FLA"
        if k=="TBR, TBD":
            k="TBD"
        if k=="WSN, MON":
            k="MON"

        h2h(k)

for rows in tables.findAll('tr'):

    for cells in rows.findAll('td')[2:3]:

        m = cells.text
        if m == "LAA, ANA":
            m = "ANA"
        if m == "MIA, FLA":
            m = "FLA"
        if m == "TBR, TBD":
            m = "TBD"
        if m == "WSN, MON":
            m = "MON"
        for rows in tables.findAll('tr'):

            for cells in rows.findAll('td')[2:3]:

                k=cells.text
                if k=="LAA, ANA":
                    k="ANA"
                if k=="MIA, FLA":
                    k="FLA"
                if k=="TBR, TBD":
                    k="TBD"
                if k=="WSN, MON":
                    k="MON"

                h3h(m,k)



#print(dataCmptly)
header = "RK,Franchise,Tm,G,W,L,W-L%,RS,RA,hmW-L,rdW-L, "+"\n"
file = open(os.path.expanduser("Baseball.csv"), "wb")
file.write(bytes(header))
file.write(bytes(dataCmptly))

