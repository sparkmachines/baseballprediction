# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

from bs4 import BeautifulSoup
import os
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
import urllib
import urllib.request

app = Flask(__name__)
app.secret_key = 'keyForScrap'

@app.route('/')
def homepage():
    return render_template("yearandteam.html")

@app.route('/', methods=["GET", "POST"])
def mainpage():
    message = None
    if request.method == "POST":
        Team_1 = request.form['Team1']
        Team_2 = request.form['Team2']
        FromDate = str(request.form['From'])
        ToDate = str(request.form['To'])
        #flash(Team_1)
        webScrapping(Team_1,Team_2,FromDate,ToDate)
        message = "Done"       
        #message = "Result: \n " + team2teamMultiply(Team_1, Team_2, FromDate, ToDate)
    return render_template("yearandteam.html", message=message)
    
def webScrapping(team1,team2,yearFrom,yearTo):
    print (team1 +team2+yearFrom+yearTo)
    link = "http://www.baseball-reference.com/games/head2head-games.cgi?team1=" + team1+ "&team2=" +team2+ "&from=" + yearFrom + "&to=" +yearTo + "&submit=Submit"
    #print(link)
    toOpenUrl=urllib.request.urlopen(link)
    #print (toOpenUrl)
    urlLink=BeautifulSoup(toOpenUrl, "html.parser")
    tables=urlLink.find('table', id='game_results')
    dataCmptly=""
    for rows in tables.findAll('tr'):
        cellInRow = ""
        for cells in rows.findAll('td'):
            cellInRow=cellInRow+","+cells.text
            # print(cellInRow)
        dataCmptly=dataCmptly+"\n"+cellInRow
    print(dataCmptly)
    header = "Date,Standings,Team,Venue,Opposition,Result,Rs,Runs Allowed,Home Runs,Homes Runs Opp,E,E-Opp,Win,Loss,Inn,Win_Person,LosePerson,Save"+"\n"
    file = open(os.path.expanduser("TeamDataScrap.csv"), "wb")
    file.write(bytes(header, encoding="ascii", errors="ignore"))
    file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))  
if __name__ == "__main__":
    #print("Hello World")
    app.run()

