from bs4 import BeautifulSoup
import os
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
import urllib
import urllib.request
app = Flask(__name__)
app.secret_key = 'keyForScrap'

@app.route('/')
def homepage():
    return render_template("Dummy.html")
@app.route('/', methods=["GET", "POST"])
def mainpage():
    message = None
    if request.method == "POST":
#        Player_1 = request.form['plyr1']
#        Player_2 = request.form['plyr2']
#        Player_3 = request.form['plyr3']
#        FromDate = str(request.form['From'])
#        ToDate = str(request.form['To'])
         Year = request.form['year']
         Statsistics = request.form['stats']
        #flash(Team_1)
        webScrapping()
        message = "Done" 
    return render_template("yearandteam.html", message=message)
def webScrapping(Self,year,Stats):
    link = "http://www.baseball-reference.com/leagues/MLB/" + year + "-standard-" + Stats + ".shtml"
    toOpenUrl=urllib.request.urlopen(link)
    print(link)
    urlLink=BeautifulSoup(toOpenUrl, "html.parser")
    if(Stats =="pitching"):
        tables=urlLink.find('table', id='players_standard_pitching')
    else:
        tables = urlLink.find('table', id='players_standard_batting')
    dataCmptly = ""
    for rows in tables.findAll('tr'):
        cellInRow = ""
        for cells in rows.findAll('td'):
            cellInRow = cellInRow + "," + cells.text
        dataCmptly = dataCmptly + "\n" + cellInRow
        print(dataCmptly)
    header = "Name,Age,Tm,Lg,W,L,W-L%,ERA,G,GS,GF,CG,SHO,SV,IP,H,R,ER,HR,BB,IBB,SO,HBP,BK,WP,BF,ERA+,FIP,WHIP,H9,HR9,BB9,SO9,SO/W"+"\n"
    file = open(os.path.expanduser("playerdata.csv"), "wb")
    file.write(bytes(header, encoding="ascii", errors="ignore"))
    file.write(bytes(dataCmptly, encoding="ascii", errors="ignore"))

if __name__ == "__main__":
    print("Hello World")
