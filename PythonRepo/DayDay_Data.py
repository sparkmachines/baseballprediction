from bs4 import BeautifulSoup, Comment
import os
import urllib.request
header = "Date(MM/DD/YYYY),Team_Id,Dummy,Batting,AR,R,H,RBI,BB,SO,PA,BA,OBP,SLG,OPS,Pit,Str,WPA,aLI,WPA+,WPA-,RE24,PO,A"+"\n"
file = open(os.path.expanduser("DaytoDay_Data.csv"), "wb")
file.write(bytes(header, encoding="ascii", errors="ignore"))
year=["2001""2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016"]
day=["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
month=["03","04","05","06","07","08","09","10"]
teams=["LAN/LAN","TBA/TBA","BAL/BAL","ARI/ARI","BOS/BOS","ATL/ATL","CHW/CHW","CIN/CIN","CHC/CHC","SLN/SLN","CLE/CLE","COL/COL","DET/DET","FLO/FLO","HOU/HOU","KCR/KCR","TOR/TOR","TEX/TEX","TBD/TBD","SIT/SIT","SEA/SEA","SFG/SFG","SDP/SDP",
"PIT/PIT","PHI/PHI","OAK/OAK","NYY/NYY","NYM/NYM","MIN/MIN","MIL/MIL","LAD/LAD","ANA/ANA", "WAS/WAS","TBA/TBA","LAN/LAN","BAL/BAL","ARI/ARI""BOS/BOS","ATL/ATL","CHW/CHW","CIN/CIN","CHC/CHC","SLN/SLN","CLE/CLE","COL/COL","DET/DET","FLO/FLO","HOU/HOU","KCR/KCR"]

for y in range(len(year)):
    for t in range(len(teams)):
        for j in range(len(month)):
            for k in range(len(day)):
                url = "http://www.baseball-reference.com/boxes/"+teams[t]+year[y]+month[j]+day[k]+"0.shtml"
                print(url)
                tableId=["LoAngelesDodgersBatting "]

                page=urllib.request.urlopen(url)
                soup = BeautifulSoup(page,"html.parser")
                tables=soup.find('table', id=tableId)
                dataCmptly=""
                tablesplit=tableId[0:-7]
                try:
                    for rows in tables.findAll('tr'):
                        cellInRow = ""
                        for cells in rows.findAll('td'):
                            cellInRow = cellInRow + "," + cells.text
                        cellInRowWithDate = day[k]+"/"+month[j] +"/" + year[y] + "," + tablesplit + "," + cellInRow+"\n"
                        print(cellInRowWithDate)
                        file.write(bytes(cellInRowWithDate, encoding="ascii", errors="ignore"))
                except:
                    print("")