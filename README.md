# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* **Quick summary:** This repository is to store our work for the Graduate Directed Project, i.e., Baseball Prediction. The project aims to build a supervised machine learning model to predict the win/lose probability for the baseball match, the model is being built by using  Azure machine learning studio. The classification of win/loss of baseball match will be classified by multiclass neural networks algorithm. We train the model after preprocessing, transforming in ETL, of web scrapping data. The model has been trained by k-fold cross validation. The model is being validated and tested by different sets from training sets.


### How do I get set up? ###

* ** Summary of set up:** In order to develop the baseball prediction system, download Netbeans IDE for developing the website, Pycharms IDE for writing Python scripts, Pentaho tool for ETL transformations and an account in Azure Cloud.
All those softwares can be downloaded and installed using the following steps:

1. Netbeans IDE: 
   Step 0: Install JDK
   To use NetBeans for Java programming, you need to first install Java Development Kit (JDK). See "JDK - How to          Install".
   Step 1: Download
   Download "NetBeans IDE" installer from http://netbeans.org/downloads/index.html. There are many "bundles"   available. For beginners, choose the "Java SE" (e.g., "netbeans-8.1-javase-windows.exe").
Step 2: Run the Installer
   Run the downloaded installer.

2. Pycharms IDE: 
  Step 0: You can download the latest version of PyCharm from the JetBrains website http://www.jetbrains.com/pycharm/download/
  Step 1: Run the .exe file and follow the instructions of PyCharm Setup wizard.

3. Pentaho: 
  Step 0: You can download Pentaho tool from the Sourceforge website https://sourceforge.net/projects/pentaho/
  Step 1: Run the .exe file and install the software.
  Step 2: Open the spoon.bat file present in the libraries in order to open the tool.

4. Azure Cloud:
   Step 0: You can create an account in the Azure Studio from the following link https://studio.azureml.net/ and can create models in it.

5. Oracle database: 
   Step 0: You can download Oracle database 11g in the following link http://www.oracle.com/technetwork/database/enterprise-edition/downloads/112010-win32soft-098987.html

* **Configuration:** In order to develop our system, a computer should has the following configuration.
  8GB RAM, 
  i3 processor and 
  250GB Hard disk
    
* **Dependencies:** Our system depends on Pentaho tool for cleaning the data and to do some transformations. It also depends on Azure Cloud. In order to develop a model in Azure cloud, one has to have an account in Azure Cloud.

* **Database configuration:** We have used Oracle 12c for the front end and SQL Server for the back end.
 
 1. Oracle 12c: You can use Oracle Database Configuration Assistant to:

    Create a default or customized database

    Configure an existing database to use Oracle products

    Create Automatic Storage Management disk groups

    Generate a set of shell and SQL scripts that you can inspect, modify, and run at a later time to create a database

    To start Oracle Database Configuration Assistant, run the following command:

    $ $ORACLE_HOME/bin/dbca
    For information about the command-line options available with Oracle Database Configuration Assistant, use the -   help or -h command-line arguments as follows:

    $ $ORACLE_HOME/bin/dbca -help


* **Deployment instructions:** In order to run our system, open Netbeans IDE and open the project from the source folder and run the intro.jsp file. This will display the webpage.

### Contribution guidelines ###

* **Writing tests:** We are testing the system with all the possible test cases rather than writing the tests in the code. The reults are given in the test reports.

* **Code review:** We have peer reviewed the code written by our teammates i.e., code written by one team member is reviewd by the other team member.


### Who do I talk to? ###

* **Repo owner or admin:** All the team members are admins. i.e., Maruthi Prasad Chava, Pavan Kanumuri, Lahari Surneni, Jhansi Mora, Goutham Kumar Karanam, Niharika Nallappagari, Vamsy Pendyala