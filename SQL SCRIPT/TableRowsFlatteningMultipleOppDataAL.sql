DECLARE @cols AS NVARCHAR(MAX),

@query AS NVARCHAR(MAX),

@colNames AS NVARCHAR(MAX),

@tablename nvarchar(max)

SET @tablename ='teamPlayerTempAL'

select @cols = STUFF((SELECT ',' + 'cast(' + COLUMN_NAME + ' as nvarchar(max)) ' + COLUMN_NAME --( QUOTENAME(COLUMN_NAME+'_'+cast(id as nvarchar(max)))

from INFORMATION_SCHEMA.columns

where table_name = @tablename and column_name !='matchdate'

FOR XML PATH(''), TYPE

).value('.', 'NVARCHAR(MAX)')

,1,1,'')

print @cols

select @colNames = STUFF((SELECT ',' + COLUMN_NAME --( QUOTENAME(COLUMN_NAME+'_'+cast(id as nvarchar(max)))

from INFORMATION_SCHEMA.columns

where table_name = @tablename and column_name not like '%id' and column_name not like '%matchdate' 
					and column_name not like '%team'

FOR XML PATH(''), TYPE

).value('.', 'NVARCHAR(MAX)')

,1,1,'')

print @colNames

set @query = N'IF OBJECT_ID (N''DropLater'', N''U'') IS NOT NULL BEGIN DROP TABLE DropLater END

select teamid,matchdate,team,''Player_''+cast(dense_rank() over(partition by teamid,matchdate order by Id) as varchar(max))+''_''+Type as ColumnName,Value into DropLater from

(

select ' + @cols + ',matchdate

from '+@tablename+'

) x

UNPIVOT (Value FOR Type IN

            ('+@colNames+')) as sq_up

'

print @query

exec sp_executesql @query;

--select * from DropLater

SET @colNames = NULL

select distinct ColumnName into #tmp from DropLater

SELECT @colNames = COALESCE(@colNames + ', ', '') + ColumnName from #tmp

print @colNames

DECLARE @sql nvarchar(max) = 'SELECT matchdate,team, '+@colNames+'  into OppFlattenedPlayerStatAL FROM DropLater PIVOT ( max(Value) for ColumnName in ('+@colNames+') ) AS pvt'

print (@sql)

exec sp_executesql @sql ;

--drop table FlattenedPlayerStatAL;
select * from FlattenedPlayerStatAL;

drop table #tmp