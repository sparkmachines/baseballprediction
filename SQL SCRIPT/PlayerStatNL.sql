USE [BaseballPrediction]
GO

/****** Object:  Table [dbo].[playerStatNL]    Script Date: 11/19/2016 12:25:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[playerStatNL](
	[RK] [int] NULL,
	[Name] [varchar](17) NULL,
	[Lg] [varchar](3) NULL,
	[G] [bigint] NULL,
	[PA] [bigint] NULL,
	[AB] [bigint] NULL,
	[R] [bigint] NULL,
	[H] [bigint] NULL,
	[B2B] [bigint] NULL,
	[B3B] [bigint] NULL,
	[HR] [bigint] NULL,
	[RBI] [bigint] NULL,
	[CS] [bigint] NULL,
	[BB] [bigint] NULL,
	[SO] [bigint] NULL,
	[BA] [decimal](5, 3) NULL,
	[SLG] [decimal](5, 3) NULL,
	[OPS] [decimal](5, 3) NULL,
	[Year] [bigint] NULL,
	[tm_1] [varchar](29) NULL
) ON [PRIMARY]

GO

