USE [BaseballPrediction]
GO

/****** Object:  Table [dbo].[teamPlayerTemp1]    Script Date: 11/20/2016 10:14:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[teamPlayerTemp1](
	[id] [int] NULL,
	[teamid] [varchar](100) NULL,
	[team] [varchar](25) NULL,
	[player] [varchar](21) NULL,
	[matchdate] [datetime] NULL,
	[ba] [decimal](5, 3) NULL,
	[g] [bigint] NULL,
	[pa] [bigint] NULL,
	[ab] [bigint] NULL,
	[r] [bigint] NULL,
	[hr] [bigint] NULL,
	[slg] [decimal](5, 3) NULL,
	[ops] [decimal](5, 3) NULL
) ON [PRIMARY]

GO

