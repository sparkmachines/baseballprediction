USE [BaseballPrediction]
GO

/****** Object:  Table [dbo].[headtoheadmatchesNL]    Script Date: 11/19/2016 10:52:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[headtoheadmatchesNL](
	[Date] [datetime] NULL,
	[Position] [varchar](9) NULL,
	[Tm] [varchar](50) NULL,
	[Venue] [varchar](4) NULL,
	[Opp] [varchar](50) NULL,
	[Results] [varchar](1) NULL,
	[RS] [bigint] NULL,
	[RA] [bigint] NULL,
	[H] [bigint] NULL,
	[Hopp] [bigint] NULL,
	[E] [bigint] NULL,
	[Eopp] [bigint] NULL,
	[W] [bigint] NULL,
	[L] [bigint] NULL,
	[Inn] [bigint] NULL,
	[Win] [varchar](16) NULL,
	[Loss] [varchar](13) NULL,
	[Save] [varchar](13) NULL,
	[Year] [int] NULL,
	[TmLg] [varchar](50) NULL,
	[OppLg] [varchar](50) NULL
) ON [PRIMARY]

GO

