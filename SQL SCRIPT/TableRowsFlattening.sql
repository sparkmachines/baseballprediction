DECLARE @cols AS NVARCHAR(MAX),

@query AS NVARCHAR(MAX),

@colNames AS NVARCHAR(MAX),

@tablename nvarchar(max)

SET @tablename ='StdPlayer'

select @cols = STUFF((SELECT ',' + 'cast(' + COLUMN_NAME + ' as nvarchar(max)) ' + COLUMN_NAME --( QUOTENAME(COLUMN_NAME+'_'+cast(id as nvarchar(max)))

from INFORMATION_SCHEMA.columns

where table_name = @tablename

FOR XML PATH(''), TYPE

).value('.', 'NVARCHAR(MAX)')

,1,1,'')

select @colNames = STUFF((SELECT ',' + COLUMN_NAME --( QUOTENAME(COLUMN_NAME+'_'+cast(id as nvarchar(max)))

from INFORMATION_SCHEMA.columns

where table_name = @tablename and column_name!='id'

FOR XML PATH(''), TYPE

).value('.', 'NVARCHAR(MAX)')

,1,1,'')

set @query = N'IF OBJECT_ID (N''DropLater'', N''U'') IS NOT NULL BEGIN DROP TABLE DropLater END

select ''Player_''+Id+''_''+Type as ColumnName,Value into DropLater from

(

select ' + @cols + '

from '+@tablename+'

) x

UNPIVOT (Value FOR Type IN

            ('+@colNames+')) as sq_up

'

print @query

exec sp_executesql @query;

SET @colNames = NULL

SELECT @colNames = COALESCE(@colNames + ', ', '') + ColumnName from DropLater

DECLARE @sql nvarchar(max) = 'SELECT '+@colNames+' FROM DropLater PIVOT ( max(Value) for ColumnName in ('+@colNames+') ) AS pvt'

print (@sql)

exec sp_executesql @sql;